rust-ashpd (0.10.2-3) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@debian.org>  Thu, 13 Feb 2025 16:38:21 +0100

rust-ashpd (0.10.2-2) experimental; urgency=medium

  * Team upload.
  * Package ashpd 0.10.2 from crates.io using debcargo 2.7.6
  * Remove wayland-protocols hunk from relax-deps, since the new version is
    available

 -- James McCoy <jamessan@debian.org>  Tue, 11 Feb 2025 00:08:09 +0100

rust-ashpd (0.10.2-1) experimental; urgency=medium

  * Package ashpd 0.10.2 from crates.io using debcargo 2.7.6
  * Refresh patches
  * Mark backend feature as flaky

 -- Matthias Geiger <werdahias@debian.org>  Fri, 17 Jan 2025 00:11:23 +0100

rust-ashpd (0.9.1-4) unstable; urgency=medium

  * Team upload.
  * Package ashpd 0.9.1 from crates.io using debcargo 2.7.6
  * Drop relax-deps patch

 -- James McCoy <jamessan@debian.org>  Sun, 09 Feb 2025 19:45:43 -0500

rust-ashpd (0.9.1-3) unstable; urgency=medium

  * Team upload
  * Package ashpd 0.9.1 from crates.io using debcargo 2.6.1
  * Remove patch avoiding rust-reis

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sat, 14 Sep 2024 16:51:19 -0400

rust-ashpd (0.9.1-2) unstable; urgency=medium

  * Team upload.
  * Package ashpd 0.9.1 from crates.io using debcargo 2.6.1
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 27 Aug 2024 14:05:48 -0400

rust-ashpd (0.9.1-1) experimental; urgency=medium

  * Package ashpd 0.9.1 from crates.io using debcargo 2.6.1
  * Refresh patches for new upstream 

 -- Matthias Geiger <werdahias@debian.org>  Thu, 22 Aug 2024 16:07:46 +0200

rust-ashpd (0.8.1-2) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 14 Jul 2024 16:02:25 +0200

rust-ashpd (0.8.1-1) experimental; urgency=medium

  * Package ashpd 0.8.1 from crates.io using debcargo 2.6.1
  * Drop wayland patch, obsoleted
  * Add patch for raw-window-handle
  * New upstream release (Closes: #1074396)

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 07 Jul 2024 13:23:49 +0200

rust-ashpd (0.6.7-2) experimental; urgency=medium

  * Team upload.
  * Package ashpd 0.6.7 from crates.io using debcargo 2.6.1
  * Bump wayland crates to 0.31

 -- James McCoy <jamessan@debian.org>  Thu, 28 Dec 2023 17:00:21 -0500

rust-ashpd (0.6.7-1) unstable; urgency=medium

  * Package ashpd 0.6.7 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 22 Oct 2023 23:02:20 +0200

rust-ashpd (0.6.5-1) unstable; urgency=medium

  * Team upload
  * Package ashpd 0.6.5 from crates.io using debcargo 2.6.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 16 Oct 2023 18:07:14 -0400

rust-ashpd (0.6.2-2) unstable; urgency=medium

  * Team upload
  * Package ashpd 0.6.2 from crates.io using debcargo 2.6.0
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 15:15:42 -0400

rust-ashpd (0.6.2-1) experimental; urgency=medium

  * Team upload.
  * Package ashpd 0.6.2 from crates.io using debcargo 2.6.0
  * Drop obsolete relax-deps.diff

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 27 Sep 2023 15:20:32 -0400

rust-ashpd (0.5.0-1) unstable; urgency=medium

  * Package ashpd 0.5.0 from crates.io using debcargo 2.6.0
  * Drop drop-features patch, no longer needed
  * Add relax-deps.diff as gtk 0.7 isn't packaged yet

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 07 Aug 2023 22:34:57 +0200

rust-ashpd (0.4.0-4) unstable; urgency=medium

  * Package ashpd 0.4.0 from crates.io using debcargo 2.6.0
  * Removed inactive uploader, added my new mail address
  * Dropped relax-deps.diff patch as the newer gtk is available

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 02 Aug 2023 00:35:10 +0200

rust-ashpd (0.4.0-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * Disable dependency on gdkwayland crate and features that depend on it.
    They can be re-enabled if/when the dependencies are available.
  * Establish baseline for tests.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 04 Jul 2023 23:35:53 +0000

rust-ashpd (0.4.0-2) unstable; urgency=medium

  * Package ashpd 0.4.0 from crates.io using debcargo 2.6.0
  * Upload to unstable

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Tue, 27 Jun 2023 07:40:11 +0200

rust-ashpd (0.4.0-1) experimental; urgency=medium

  * Package ashpd 0.4.0 from crates.io using debcargo 2.6.0
  * Added myself to Uploaders
  * Rebased patches

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Mon, 26 Jun 2023 10:47:25 +0200

rust-ashpd (0.1.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * fix unsatisfiable dependency and failure to build from source:
    + extend patch and dependencies
      to link against librust-gdk-0.14+default-dev
      (not librust-gdk-0.13+default-dev never in Debian)

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 06 Apr 2022 21:38:58 +0200

rust-ashpd (0.1.0-1) unstable; urgency=medium

  * Package ashpd 0.1.0 from crates.io using debcargo 2.4.4

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sun, 06 Feb 2022 20:23:00 +0100
