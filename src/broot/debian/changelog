rust-broot (1.44.6-2) unstable; urgency=medium

  * Package broot 1.44.6 from crates.io using debcargo 2.7.6
  * Use dh-shell-completions to install shell completions

 -- NoisyCoil <noisycoil@tutanota.com>  Wed, 29 Jan 2025 00:21:24 +0100

rust-broot (1.44.6-1) unstable; urgency=medium

  * Package broot 1.44.6 from crates.io using debcargo 2.7.6
  * Downgrade git2 dependency to v0.19
  * Do not depend on clap_mangen: the manpage is generated upstream
  * d/patches: refresh

 -- NoisyCoil <noisycoil@tutanota.com>  Tue, 28 Jan 2025 01:05:16 +0100

rust-broot (1.44.5-1) unstable; urgency=medium

  * Package broot 1.44.5 from crates.io using debcargo 2.7.5
  * d/copyright: update copyright years
  * d/patches: refresh

 -- NoisyCoil <noisycoil@tutanota.com>  Wed, 08 Jan 2025 20:29:39 +0100

rust-broot (1.44.3-1) unstable; urgency=medium

  * Package broot 1.44.3 from crates.io using debcargo 2.7.5
  * d/patches: refresh

 -- NoisyCoil <noisycoil@tutanota.com>  Sat, 28 Dec 2024 14:54:11 -0500

rust-broot (1.44.2-6) unstable; urgency=medium

  * Package broot 1.44.2 from crates.io using debcargo 2.7.5
  * Bump lfs-core to v0.12, termimad to v0.31 and xterm-query to v0.5

 -- NoisyCoil <noisycoil@tutanota.com>  Sun, 22 Dec 2024 18:16:35 +0100

rust-broot (1.44.2-5) unstable; urgency=medium

  * Package broot 1.44.2 from crates.io using debcargo 2.7.5
  * Fix fish shell completion file path, thanks to David Adam

 -- NoisyCoil <noisycoil@tutanota.com>  Fri, 20 Dec 2024 07:47:26 +0100

rust-broot (1.44.2-4) unstable; urgency=medium

  * Package broot 1.44.2 from crates.io using debcargo 2.7.5
  * Bump lazy-regex dependency to v3

 -- NoisyCoil <noisycoil@tutanota.com>  Fri, 06 Dec 2024 00:29:39 +0100

rust-broot (1.44.2-3) unstable; urgency=medium

  * Package broot 1.44.2 from crates.io using debcargo 2.7.4
  * Set the manpage's year from d/changelog's entry to make the build
    reproducible. Thanks to Chris Lamb (Closes: #1088238)

 -- NoisyCoil <noisycoil@tutanota.com>  Fri, 29 Nov 2024 19:52:14 +0100

rust-broot (1.44.2-2) unstable; urgency=medium

  * Package broot 1.44.2 from crates.io using debcargo 2.7.4
  * Bump image dependency to v0.25 and trash dependency to v5
  * d/patches:
    - simplify relax-deps.diff
    - refresh other patches

 -- NoisyCoil <noisycoil@tutanota.com>  Sat, 23 Nov 2024 14:21:42 +0100

rust-broot (1.44.2-1) unstable; urgency=medium

  * Package broot 1.44.2 from crates.io using debcargo 2.7.1 (Closes: #948483)

  [ Sylvestre Ledru ]
  * Team upload.
  * Package broot 1.0.5 from crates.io using debcargo 2.4.3

  [ Emmanuel Arias ]
  * Package broot 0.13.1 from crates.io using debcargo 2.4.3-alpha.0

 -- NoisyCoil <noisycoil@tutanota.com>  Sat, 26 Oct 2024 21:12:53 +0200
