Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: escaper
Upstream-Contact:
 dignifiedquire <dignifiedquire@gmail.com>
 Viktor Dahl <pazaconyoman@gmail.com>
Source: https://github.com/dignifiedquire/rust-escaper

Files: *
Copyright:
 2019-2024 dignifiedquire <dignifiedquire@gmail.com>
 2013-2014 Viktor Dahl <pazaconyoman@gmail.com>
License: Apache-2.0  or  MIT  or  MPL-2.0
Comment:
 This package was originally published by Viktor Dahl as html-escape, who
 worked on it from 2013 to 2014. In 2019, it was forked by dignifiedquire
 as escaper. The README file sets the license as "Apache-2.0 or MIT", but
 Cargo.toml has always included MPL-2.0 as well (since the html-escape days)
 so I'm going with that (also created an issue on the repo)

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Badri Sunderarajan <badrihippo@disroot.org>
License: Apache-2.0  or  MIT  or  MPL-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: MPL-2.0
 Debian systems provide the MPL 2.0 in /usr/share/common-licenses/MPL-2.0
