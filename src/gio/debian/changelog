rust-gio (0.20.9-1) unstable; urgency=medium

  * Team upload
  * Package gio 0.20.9 from crates.io using debcargo 2.7.6

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 17 Feb 2025 15:59:16 -0500

rust-gio (0.20.7-2) unstable; urgency=medium

  * Team upload
  * Package gio 0.20.7 from crates.io using debcargo 2.7.6
  * Unmark v2_84 test as flaky

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sat, 15 Feb 2025 08:40:12 -0500

rust-gio (0.20.7-1) unstable; urgency=medium

  * Package gio 0.20.7 from crates.io using debcargo 2.7.6
  * New upstream release (Closes: #1095387)
  * Mark v2_84 feature as flaky

 -- Matthias Geiger <werdahias@debian.org>  Sun, 09 Feb 2025 17:46:37 +0100

rust-gio (0.20.4-1) unstable; urgency=medium

  * Package gio 0.20.4 from crates.io using debcargo 2.7.1

 -- Matthias Geiger <werdahias@debian.org>  Wed, 23 Oct 2024 21:56:10 +0200

rust-gio (0.20.1-2) unstable; urgency=medium

  * Team upload.
  * Package gio 0.20.1 from crates.io using debcargo 2.6.1
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 26 Aug 2024 17:14:27 -0400

rust-gio (0.20.1-1) experimental; urgency=medium

  * Team upload.
  * Package gio 0.20.1 from crates.io using debcargo 2.6.1
  * Depend on gir-rust-code-generator 0.20
  * Add patch to relax serial-test dependency

 -- Matthias Geiger <werdahias@debian.org>  Fri, 16 Aug 2024 12:51:36 +0200

rust-gio (0.19.5-1) unstable; urgency=medium

  * Package gio 0.19.5 from crates.io using debcargo 2.6.1
  * Stop marking v2.80 feature as broken
  * Depend on gir-rust-code-generator 0.19.1

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 04 May 2024 19:39:59 +0200

rust-gio (0.19.4-1) experimental; urgency=medium

  * Team upload
  * Package gio 0.19.4 from crates.io using debcargo 2.6.1
  * debian/rules: Add GModule-2.0.gir to fix build with glib 2.80

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 17 Apr 2024 09:28:20 -0400

rust-gio (0.19.2-1) experimental; urgency=medium

  * Package gio 0.19.2 from crates.io using debcargo 2.6.1
  * Remove hard dependecy on rustc
  * Bump version for gir-rust-code-generator
  * Marked v2_80 feature as broken

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 19 Feb 2024 00:41:59 +0100

rust-gio (0.18.4-1) unstable; urgency=medium

  * Package gio 0.18.4 from crates.io using debcargo 2.6.1

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 31 Dec 2023 00:10:45 +0100

rust-gio (0.18.3-1) unstable; urgency=medium

  * Stop patching gir-format-check dependency
  * Package gio 0.18.3 from crates.io using debcargo 2.6.1
  * Drop patch for serial-test; not needed anymore
  * Versioned dependency on gir-rust-code-generator
  * Updated debcargo.toml in order for the additional dependencies to
    show up as build-depends only

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 08 Dec 2023 04:26:32 +0100

rust-gio (0.18.2-3) unstable; urgency=medium

  * Team upload
  * Package gio 0.18.2 from crates.io using debcargo 2.6.0
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 15:36:08 -0400

rust-gio (0.18.2-2) experimental; urgency=medium

  * Specifiy dependency on rustc 1.70

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 16 Sep 2023 12:55:34 +0200

rust-gio (0.18.2-1) experimental; urgency=medium

  * Package gio 0.18.2 from crates.io using debcargo 2.6.0
  * Regenerate source code with debian tools before build
  * Rebased patches

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 15 Sep 2023 22:54:38 +0200

rust-gio (0.17.10-1) unstable; urgency=medium

  * Package gio 0.17.10 from crates.io using debcargo 2.6.0
  * Removed inactive uploader, added my new mail address
  * Added patch to enable tests

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 02 Aug 2023 00:16:23 +0200

rust-gio (0.16.7-2) unstable; urgency=medium

  * Package gio 0.16.7 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 25 Jun 2023 20:54:17 +0200

rust-gio (0.16.7-1) experimental; urgency=medium

  * Package gio 0.16.7 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 20 May 2023 14:55:15 +0200

rust-gio (0.14.8-1) unstable; urgency=medium

  * Package gio 0.14.8 from crates.io using debcargo 2.5.0 (Closes: 1002154)

  [ Henry-Nicolas Tourneur ]
  * Team upload.
  * Package gio 0.14.8 from crates.io using debcargo 2.4.4
  * d/patches: removed patch remove-futures-preview-feature.diff

 -- Peter Michael Green <plugwash@debian.org>  Tue, 28 Dec 2021 20:34:32 +0000

rust-gio (0.7.0-1) unstable; urgency=medium

  * Package gio 0.7.0 from crates.io using debcargo 2.3.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Thu, 25 Jul 2019 21:16:06 -0700

rust-gio (0.5.1-2) unstable; urgency=medium

  * Package gio 0.5.1 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Wed, 26 Dec 2018 09:50:43 +0100

rust-gio (0.5.1-1) unstable; urgency=medium

  * Package gio 0.5.1 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Fri, 07 Dec 2018 19:29:34 -0800
