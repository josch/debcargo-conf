rust-plotters (0.3.7-2) unstable; urgency=medium

  * Team upload.
  * Package plotters 0.3.7 from crates.io using debcargo 2.7.2
  * Bump font-kit dependency to v0.14

 -- NoisyCoil <noisycoil@tutanota.com>  Sat, 16 Nov 2024 10:51:37 +0000

rust-plotters (0.3.7-1) unstable; urgency=medium

  * Team upload.
  * Package plotters 0.3.7 from crates.io using debcargo 2.7.2
  * Bump image to v0.25
  * Update d/copyright
  * Stop overlaying debcargo's d/control
  * d/patches:
    - refresh patches
    - split wasm dependency removal to its own patch
    - disable-examples.patch: ignore doctests depending on absent test data,
      manually disable all examples, incompatible with feature testing

 -- NoisyCoil <noisycoil@tutanota.com>  Sat, 09 Nov 2024 18:00:18 +0100

rust-plotters (0.3.5-4) unstable; urgency=medium

  * Team upload.
  * Upgrade ttf-parser to v0.24
  * Downgrade itertools to 0.10 while 0.12 remains in experimental

 -- NoisyCoil <noisycoil@tutanota.com>  Tue, 10 Sep 2024 10:48:24 +0100

rust-plotters (0.3.5-3) experimental; urgency=medium

  * Team upload.
  * Package plotters 0.3.5 from crates.io using debcargo 2.6.1
  * Upgrade itertools to 0.12.

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Sat, 30 Mar 2024 23:02:56 -0400

rust-plotters (0.3.5-2) unstable; urgency=medium

  * Team upload.
  * Package plotters 0.3.5 from crates.io using debcargo 2.6.0
  * Fix autopkgtest
    + Mark tests for the colormaps feature as broken, they fail to build with
      --no-default-features.
    + Gate some tests on the "ttf" feature.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 18 Jul 2023 20:41:45 +0000

rust-plotters (0.3.5-1) unstable; urgency=medium

  * Team upload.
  * Package plotters 0.3.5 from crates.io using debcargo 2.6.0
  * Bump criterion dev-dependency to 0.5

 -- James McCoy <jamessan@debian.org>  Sun, 16 Jul 2023 09:40:29 -0400

rust-plotters (0.3.4-3) unstable; urgency=medium

  * Team upload.
  * Package plotters 0.3.4 from crates.io using debcargo 2.6.0
  * Really fix the author list (Closes: #1025661)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 10 Dec 2022 15:27:12 +0000

rust-plotters (0.3.4-2) unstable; urgency=medium

  [ Blair Noctis ]
  * Fix the author list (Closes: #1025661)

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 07 Dec 2022 09:10:41 +0100

rust-plotters (0.3.4-1) unstable; urgency=medium

  * Team upload.
  * Package plotters 0.3.4 from crates.io using debcargo 2.5.0

  [ Jelmer Vernooĳ ]
  * Package plotters 0.3.1 from crates.io using debcargo 2.5.0

 -- Blair Noctis <n@sail.ng>  Tue, 6 Dec 2022 11:14:48 +0100
