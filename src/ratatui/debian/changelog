rust-ratatui (0.28.1-7) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.28.1 from crates.io using debcargo 2.7.0
  * Skip some tests that are over-sensitive to the precise behaviour
    of unicode-width.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 06 Oct 2024 14:00:50 +0000

rust-ratatui (0.28.1-6) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.28.1 from crates.io using debcargo 2.7.0
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 03 Oct 2024 12:08:43 +0000

rust-ratatui (0.28.1-5) experimental; urgency=medium

  * Team upload.
  * Package ratatui 0.28.1 from crates.io using debcargo 2.6.1
  * Drop downgrade-itertools-0.10.patch

 -- Peter Michael Green <plugwash@debian.org>  Sat, 21 Sep 2024 22:56:15 +0000

rust-ratatui (0.28.1-4) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.28.1 from crates.io using debcargo 2.6.1
  * Tighten itertools dependency, the patch for 0.10 breaks builds with 0.13

 -- Peter Michael Green <plugwash@debian.org>  Sat, 21 Sep 2024 22:47:26 +0000

rust-ratatui (0.28.1-3) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.28.1 from crates.io using debcargo 2.6.1
  * Add breaks on old version of librust-tui-react-dev.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 05 Sep 2024 15:24:53 +0000

rust-ratatui (0.28.1-2) unstable; urgency=medium

  * Team upload.
  * Disable document-features to fix build failure

 -- Blair Noctis <ncts@debian.org>  Sun, 01 Sep 2024 00:43:02 +0000

rust-ratatui (0.28.1-1) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.28.1 from crates.io using debcargo 2.6.1
  * Disable examples to save build time
  * Further disable unused and/or unuavailable deps
  * Downgrade to itertools 0.10
  * Drop reinstate-feature-comments.patch, these comments only make sense in
  the order of original feature table, which crates.io "canonicalized"
  * Drop outdated patches

 -- Blair Noctis <ncts@debian.org>  Sat, 31 Aug 2024 18:06:57 +0000

rust-ratatui (0.25.0-2) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.25.0 from crates.io using debcargo 2.6.1
  * Upload to unstable (Closes: #1057254, #1074587)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 20 Jul 2024 20:44:53 +0000

rust-ratatui (0.25.0-1) experimental; urgency=medium

  * Team upload.
  * Package ratatui 0.25.0 from crates.io using debcargo 2.6.1
  * Update patches for new upstream.
  * Eliminate use of "stability" crate.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 08 Jul 2024 06:19:38 +0000

rust-ratatui (0.23.0-5) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.23.0 from crates.io using debcargo 2.6.1
  * Fix dep on strum 0.25 to 0.26

 -- Blair Noctis <n@sail.ng>  Tue, 11 Jun 2024 21:11:19 +0200

rust-ratatui (0.23.0-4) unstable; urgency=medium

  * Package ratatui 0.23.0 from crates.io using debcargo 2.6.1
  * Relax dependency on itertools.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 10 Feb 2024 03:49:06 +0000

rust-ratatui (0.23.0-3) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.23.0 from crates.io using debcargo 2.6.1
  * Stop patching strum dependency.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 12 Dec 2023 21:40:29 +0000

rust-ratatui (0.23.0-2) unstable; urgency=medium

  * Team upload.
  * Package ratatui 0.23.0 from crates.io using debcargo 2.6.0

 -- Peter Michael Green <plugwash@debian.org>  Thu, 23 Nov 2023 04:42:03 +0000

rust-ratatui (0.23.0-1) experimental; urgency=medium

  * Team upload.
  * Package ratatui 0.23.0 from crates.io using debcargo 2.6.0
  * Update relax-dep-versions.patch for new upstream and current situation
    in Debian.
  * Drop support-older-termion-versions.patch, no longer needed.
  * Move removal of cargo-husky earlier in the quilt series, because it causes
    problems testing.
  * Add upstream patch to make demo build with termion 1.
  * Reinstate feature comments that are stripped out by the "cargo publish"
    process as docuement_features needs them.
  * Remove reference to benches that are not included in the crates.io
    release.
  * Remove dev-dependency on fakeit, it's not in Debian and doesn't seem
    to be needed.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 19 Nov 2023 15:45:29 +0000

rust-ratatui (0.22.0-1) unstable; urgency=medium

  * Package ratatui 0.22.0 from crates.io using debcargo 2.6.0
  * Update debian/copyright.

 -- Johann Felix Soden <johfel@debian.org>  Sun, 23 Jul 2023 23:16:13 +0200

rust-ratatui (0.21.0-1) unstable; urgency=medium

  * Package ratatui 0.21.0 from crates.io using debcargo 2.6.0

 -- Johann Felix Soden <johfel@debian.org>  Sun, 16 Jul 2023 22:12:07 +0200
