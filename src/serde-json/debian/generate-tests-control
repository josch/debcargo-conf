#!/bin/bash

TESTDIR=src/serde-json/debian/tests
CONTROL=$TESTDIR/control

if ! [ -d $TESTDIR ]
then
    echo "This script must be called from the debcargo-conf directory" >&2
    exit 1
fi

if ! [ -f $CONTROL.debcargo.hint ]
then
    echo "$CONTROL.debcargo.hint not present" >&2
    echo "Run ./update.sh serde_json before running this script" >&2
    exit 1
fi

if ! command -v grep-dctrl >/dev/null 2>&1
then
    echo "This script requires grep-dctrl in the dctrl-tools package" >&2
    exit 1
fi

rm $CONTROL
echo "### This control file was generated by running src/serde-json/debian/generate-tests-control" >> $CONTROL
echo "### Update this file for each new crate version by rerunning the script" >> $CONTROL
echo >> $CONTROL

# Remove two broken tests: alloc is not meant to work on its own,
# and serde_json requires either std or alloc to compile.
# Also add the std feature to every test to allow for compilation.
grep-dctrl -F Test-Command -v --pattern="--features alloc" $CONTROL.debcargo.hint | \
    grep-dctrl -F Test-Command -v -e --pattern="--no-default-features$" /dev/stdin | \
    sed -e 's/--features \(arbitrary_precision\|float_roundtrip\|indexmap\|raw_value\|unbounded_depth\)/--features \1,std/' >> $CONTROL

# Upstream also tests the float_roundtrip,arbitrary_precision combination
grep-dctrl -F Test-Command --pattern="--features float_roundtrip" $CONTROL.debcargo.hint | \
    sed -e 's/--features float_roundtrip/--features float_roundtrip,arbitrary_precision,std/; s/:float_roundtrip/:float_roundtrip+arbitrary_precision/' >> $CONTROL
