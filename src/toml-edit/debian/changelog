rust-toml-edit (0.22.22-1) unstable; urgency=medium

  * Team upload.
  * Package toml_edit 0.22.22 from crates.io using debcargo 2.7.6
  * Drop relax-deps patch

 -- James McCoy <jamessan@debian.org>  Fri, 14 Feb 2025 20:20:15 -0500

rust-toml-edit (0.22.20-1) unstable; urgency=medium

  * Team upload.
  * Package toml_edit 0.22.20 from crates.io using debcargo 2.6.1
  * Drop winnow-0.6.patch, no longer needed.
  * Update remaining patches for new upstream.
  * Relax dev-dependency on libtest-mimic
  * Disable tests that use snapbox, Debian's version of snapbox is too
    different from the version upstream wants to make patching sustainable.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 08 Sep 2024 05:33:13 +0000

rust-toml-edit (0.21.0-3) unstable; urgency=medium

  * Team upload.
  * Package toml_edit 0.21.0 from crates.io using debcargo 2.6.1
  * Stop patching snapbox dependency.
  * Add patch for winnow 0.6.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 14 May 2024 03:55:02 +0000

rust-toml-edit (0.21.0-2) unstable; urgency=medium

  * Team upload.
  * Package toml_edit 0.21.0 from crates.io using debcargo 2.6.1

 -- Peter Michael Green <plugwash@debian.org>  Tue, 30 Jan 2024 15:57:13 +0000

rust-toml-edit (0.21.0-1) experimental; urgency=medium

  * Team upload.
  * Package toml_edit 0.21.0 from crates.io using debcargo 2.6.0
  * Update patches for new upstream.
  * Skip a couple of tests that fail with stack overflows, these tests were
    inadvertently skipped in previous versions of the Debian package.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 15 Nov 2023 00:13:55 +0000

rust-toml-edit (0.19.14-1) unstable; urgency=medium

  * Team upload.
  * Package toml_edit 0.19.14 from crates.io using debcargo 2.6.0
  * Drop drop-criterion.patch, drop-pretty-assertions.patch and
    drop-snapbox.patch no longer needed.
  * Rework drop-toml-test-harness.patch for new upstream, extend it to also
    Drop dependency on toml-test-data and rename it to drop-toml-test.patch.
  * Add patch relax-dep.diff to relax various dependencies.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 26 Aug 2023 12:18:03 +0000

rust-toml-edit (0.14.4-2) unstable; urgency=medium

  * Team upload.
  * Package toml_edit 0.14.4 from crates.io using debcargo 2.5.0

 -- Fabian Gruenbichler <debian@fabian.gruenbichler.email>  Sun, 23 Oct 2022 09:23:53 -0400

rust-toml-edit (0.14.4-1) unstable; urgency=medium

  * Team upload.
  * Package toml_edit 0.14.4 from crates.io using debcargo 2.5.0
  * Set collapse_features = true
  * Remove dev-dependencies that are not in Debian and disable the tests
    that depend on them so the rest of the testsuite can run.
  * Reduce dependency on kstring to major version 1 to match what is in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 19 Jun 2022 22:38:46 +0000

rust-toml-edit (0.1.5-2) unstable; urgency=medium

  * Team upload.
  * Package toml_edit 0.1.5 from crates.io using debcargo 2.4.2

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 20 Apr 2020 14:22:25 +0200

rust-toml-edit (0.1.5-1) unstable; urgency=medium

  * Package toml_edit 0.1.5 from crates.io using debcargo 2.4.2

 -- Robin Krahl <robin.krahl@ireas.org>  Thu, 06 Feb 2020 18:37:01 +0100
