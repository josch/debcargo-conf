rust-zstd (0.13.2-1) unstable; urgency=medium

  * Package zstd 0.13.2 from crates.io using debcargo 2.6.1
  * Drop temporary TargetCBlockSize patch

 -- Blair Noctis <n@sail.ng>  Sat, 20 Jul 2024 19:31:06 +0000

rust-zstd (0.13.1-2) unstable; urgency=medium

  * Team upload.
  * Temporarily patch out target block size feature only available with libzstd 1.5.6

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Mon, 17 Jun 2024 21:09:32 +0200

rust-zstd (0.13.1-1) unstable; urgency=medium

  * Really disable test requiring unavailable data
  * Add myself to uploaders
  * Package zstd 0.13.1 from crates.io using debcargo 2.6.1

 -- Blair Noctis <n@sail.ng>  Sun, 16 Jun 2024 01:23:15 +0000

rust-zstd (0.13.0-1) experimental; urgency=medium

  * Team upload.
  * Package zstd 0.13.0 from crates.io using debcargo 2.6.1 (Closes: #1056407)
  * Update remove-unavailable-features.patch with updated features

 -- Blair Noctis <n@sail.ng>  Fri, 01 Dec 2023 02:12:35 +0000

rust-zstd (0.12.1-1) unstable; urgency=medium

  * Team upload.
  * Package zstd 0.12.1+zstd.1.5.2 from crates.io using debcargo 2.6.0
  * Disable examples and remove related dev deps
  * Drop fix-bindgen.patch and fix-pkg-config.patch, unnecessary

 -- Blair Noctis <n@sail.ng>  Fri, 06 Jan 2023 17:06:25 +0100

rust-zstd (0.11.2-1) unstable; urgency=medium

  * Package zstd 0.11.2+zstd.1.5.2 from crates.io using debcargo 2.6.0 (Closes: #969609)

  [ Blair Noctis ]
  * Package zstd 0.11.2+zstd.1.5.2 from crates.io using debcargo 2.5.0
  * Remove unavailable zstd features
  * Make bindgen and pkg-config empty features as zstd-sys patch mandates them
  * Fix tests where fixtures are absent
  * Remove partial-io as it's archived upstream

  [ Sylvestre Ledru ]
  * Team upload.
  * Package zstd 0.5.3+zstd.1.4.5 from crates.io using debcargo 2.4.3

 -- Peter Michael Green <plugwash@debian.org>  Thu, 01 Dec 2022 10:16:02 +0000

rust-zstd (0.5.1-2) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package zstd 0.5.1+zstd.1.4.4 from crates.io using debcargo 2.4.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 18 Oct 2020 11:29:41 +0200

rust-zstd (0.5.1-1) unstable; urgency=medium

  * Source upload
  * Package zstd 0.5.1+zstd.1.4.4 from crates.io using debcargo 2.4.1-alpha.0

 -- Fabian Grünbichler <f.gruenbichler@proxmox.com>  Tue, 14 Jan 2020 23:07:46 +0000
